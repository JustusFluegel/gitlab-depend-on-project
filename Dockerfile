FROM node:latest

COPY . tool/

RUN cd tool && npm i && chmod +x bin.sh && npm link

CMD ["/bin/bash"]