const axios = require("axios").default;
const fs = require("fs");
const path = require("path");
const AdmZip = require("adm-zip");

const formUrlEncoded = x =>
    Object.keys(x).reduce((p, c) => p + `&${c}=${encodeURIComponent(x[c])}`, '')

const finishedStates = [
    "failed",
    "manual",
    "canceled",
    "success",
    "skipped"
]

const successfulStates = [
    "success",
    "skipped"
]

const DEFAULT_SLEEP_DELAY = 10000;

module.exports = async (args) => {
    try {
        let [PROJECT, BRANCH, SLEEP] = args;
        SLEEP = parseInt(SLEEP);
        if (isNaN(SLEEP) || SLEEP == null) SLEEP = DEFAULT_SLEEP_DELAY;
        if (PROJECT == null || PROJECT == "") throw new Error("project is an required parameter")
        if (BRANCH == null) BRANCH = "master";
        const { CI_API_V4_URL, CI_JOB_TOKEN, CI_PROJECT_PATH, API_TOKEN } = process.env;
        if (API_TOKEN == null || API_TOKEN == "") throw new Error("API Token is an required env Variable - Scopes required: api_read")
        let { id, status } = (await axios({
            method: "POST",
            baseURL: CI_API_V4_URL || "https://gitlab.com/api/v4",
            url: `projects/${encodeURIComponent(PROJECT)}/trigger/pipeline`,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            data: formUrlEncoded({
                "variables[DEPENDENCY_FROM_PROJECT]": "true",
                "variables[PARRENT_PROJECT]": CI_PROJECT_PATH,
                "token": CI_JOB_TOKEN,
                "ref": BRANCH
            })
        })).data;

        if (id == null) throw new Error("Failed to trigger the pipeline, id is null");
        while (!finishedStates.includes(status)) {
            await new Promise(r => setTimeout(r, SLEEP));
            status = (await axios({
                method: "GET",
                baseURL: CI_API_V4_URL || "https://gitlab.com/api/v4",
                url: `projects/${encodeURIComponent(PROJECT)}/pipelines/${id}`,
                headers: {
                    "PRIVATE-TOKEN": API_TOKEN
                }
            })).data.status;
            console.log(`Status of pipeline ${id} on project ${PROJECT}: ${status} : ${!finishedStates.includes(status) ? `Not finished, refreshing in ${(SLEEP / 1000).toString()} seconds` : `finished, exiting refresh loop`}`);
        }
        if (!successfulStates.includes(status)) {
            console.log(`Pipeline ${id} on project ${PROJECT} finished in an non-sucessful state: ${status}`)
            process.exit(1);
        }

        console.log(`Getting jobs of pipeline ${id} on project ${PROJECT} ...`);

        const jobs = (await axios({
            method: "GET",
            baseURL: CI_API_V4_URL || "https://gitlab.com/api/v4",
            url: `projects/${encodeURIComponent(PROJECT)}/pipelines/${id}/jobs`,
            headers: {
                "PRIVATE-TOKEN": API_TOKEN
            }
        })).data;
        if (jobs == null || !(jobs instanceof Array)) throw new Error("Failed to request Jobs!")
        await Promise.all(Object.values(jobs).map((job, i) => (async () => {
            console.log(`\t Job ${i + 1}: ${job.name}`)
            if (job.artifacts != null && job.artifacts instanceof Array && job.artifacts.length != null && job.artifacts.some(el => el.file_type != null && el.file_type
                 == "archive")) {
                console.log(`\t\t Artifacts found, creating folder (Job ${i + 1}: ${job.name}): ${PROJECT.split("/").slice(-1)[0]}/${job.name}/`)
                const artifactsPath = path.resolve(".", PROJECT.split("/").slice(-1)[0], job.name)
                const tempFolder = await fs.promises.mkdtemp(`tmp-${job.id}-`);
                const archive = path.resolve(tempFolder, "archive.zip");
                const stream = fs.createWriteStream(archive);
                const archiveStream = await axios({
                    method: "GET",
                    baseURL: CI_API_V4_URL || "https://gitlab.com/api/v4",
                    url: `projects/${encodeURIComponent(PROJECT)}/jobs/${encodeURIComponent(job.id)}/artifacts`,
                    headers: {
                        "PRIVATE-TOKEN": API_TOKEN
                    },
                    responseType: "stream"
                })
                archiveStream.data.pipe(stream);

                await new Promise((resolve, reject) => {
                    stream.on("error", reject);
                    stream.on("finish", resolve)
                })
                await fs.promises.mkdir(artifactsPath, { recursive: true });
                const zip = new AdmZip(archive);
                await new Promise((resolve, reject) => {
                    zip.extractAllToAsync(artifactsPath, true, (err) => {
                        if (err) reject(err);
                        else resolve();
                    })
                })
                console.log(`\t\t Saved Artifacts to folder (Job ${i + 1}: ${job.name}): ${PROJECT.split("/").slice(-1)[0]}/${job.name}/`)
            }
        })()))
        console.log()
        process.exit(0);
    } catch (e) {
        console.error(e);
        process.exit(1);
    }

}
