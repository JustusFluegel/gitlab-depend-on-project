build:
	docker build -t gitlab-depend-project -f Dockerfile .

run: build
	docker run -d gitlab-depend-project

shell: build
	docker run -it --rm gitlab-depend-project

build-gitlab:
	docker build -t registry.gitlab.com/justusfluegel/gitlab-depend-on-project -f Dockerfile .

publish-gitlab: build-gitlab
	docker push registry.gitlab.com/justusfluegel/gitlab-depend-on-project